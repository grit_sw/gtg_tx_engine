#!/usr/bin/python3
from logger  import logger

class Subnet():
    """
        information about subnet , e.g. transformer, is captured
        The subnet_id from the user's information is used
    """
    
    def __init__(self, subnet_id=None):
        #using the subnet_id, the params for a subnet is derived"

        self.max_capacity = 100000
        self.unused_capacity = 10000 #this is the unused capacity
        self.available = 0
        self.consumed = 0
    
    def validateBuyOrder(self, buyOrder):
        kwOrder = buyOrder["kwOrder"]
        if kwOrder > self.available:
            logger.info("Buy Order not validated")
            return False
        
        self.available -= kwOrder
        self.consumed += kwOrder
        return True
    
    def validateSellOrder(self, sellOrder):
        kwOrder = sellOrder["energy_limit"]
        if kwOrder > self.unused_capacity:
            logger.info("Sell order not validated")
            return False
        
        self.unused_capacity -= kwOrder
        self.available += kwOrder
        return True
    

        
        