#!/usr/bin/python3
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from avro import schema
import sys
from logger  import logger
import config

def Producer():
    """
        This function creates a kafka producer with the given topic name
    """
    
    broker_url = config.KAFKA_BROKER
    schema_subject = "orders"
    schema_registry = config.SCHEMA_REGISTRY
    schema_file = config.SCHEMA_FILE

    _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)

    if not sch:
        with open(schema_file, 'r+') as sc:
            avro_schema = schema.Parse(sc.read())
            schemareg = SchemaRegistry(schema_registry)
            schemareg.register(schema_subject, avro_schema)
        logger.info('New schema registration at {} with subject {}\n'.format(schema_registry, schema_subject))
        _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
    else:
        logger.info('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))
    
    avroProducer = AvroProducer({
        'bootstrap.servers': broker_url,
        'group.id': 'groupid',
        'schema.registry.url': schema_registry
    }, default_value_schema=sch)

    return avroProducer