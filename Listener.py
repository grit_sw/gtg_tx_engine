#!/usr/bin/python3

from confluent_kafka.avro import AvroConsumer
from confluent_kafka import KafkaError
from confluent_kafka.avro.serializer import SerializerError
from logger  import logger
import config

def Consumer(topic = ['new-sell-order', 'new-buy-order', 'order-complete']):
    """
        This function creates a kafka consumer with the given topic name
    """
    avroConsumer = AvroConsumer({
        'bootstrap.servers': config.KAFKA_BROKER,
        'group.id': 'groupid',
        'schema.registry.url': config.SCHEMA_REGISTRY})
    
    avroConsumer.subscribe(topic)

    return avroConsumer

def listenForOrders(avroConsumer, verbose=False):
    """
        This is used for first time processing of orders before validation
    """
    try:
        msg = avroConsumer.poll(5)

    except SerializerError as e:
        logger.info("Message deserialization failed for %s: %s", msg, e)
        exit()

    if msg is None:
        # logger.info("listening for raw orders...")
        return

    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            return
        else:
            logger.info(msg.error())
            exit()
    
    if verbose:
        logger.info("\nRaw Order: {}".format(msg.value()))
        logger.info("Topic: {}".format(msg.topic()))
        
    
    return msg

if __name__ == '__main__':
    try:
        avroConsumer = Consumer()
        while True:
            listenForOrders(avroConsumer, True)
        avroConsumer.close()
    except KeyboardInterrupt:
        pass