FROM alpine:latest

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 librdkafka-dev
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    python3-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY order.avsc Listener.py Producer.py Subnet.py TxEngine.py logger.py config.py matchOrder.py start.sh ./

RUN chmod +x start.sh
ENTRYPOINT [ "./start.sh" ]