## GTG TRANSACTION ENGINE

This is for the validation and transaction process between order creation on GTG and order processing on the meter(Prosumers).

### Required tools

1. Python >=3.5.2
2. librdkafka 
2. Confluent-Kafka == 2.11

### How to use

* Listener.py: Listens for raw orders from the GTG platform (web and mobile). NB: the repository "order_simulator" can be used to create test orders.

* Subnet.py: This is a representation of the subnet capacities.

* TxEngine.py: This is the place where the raw orders and completed orders are validated with the subnet's current capacity.

* Producer.py: This is a Kafka producer that processes validated orders and activates the prosumer(meter) NB: the repository "order_simulator" can be used to simulate the activation of prosumers after order is validated.

* build.sh: Shell script for deployment on the server

* start.sh: Shell script used to start the transaction engine