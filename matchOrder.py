#!/usr/bin/python3

orders = [
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test', 
        'rate': 10, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    },
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test', 
        'rate': 25, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    },
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test', 
        'rate': 10, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    },
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test',
        'rate': 15, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    },
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test', 
        'rate': 10, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    },
    {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': 'test', 
        'end_date_time': 'test', 
        'rate': 20, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    }
]

def matches(rate):
    count = 0
    for x in orders:
        if x['rate'] == rate:
            count += 1
    
    return count