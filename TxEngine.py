#!/usr/bin/python3
from Subnet import Subnet
from Listener import listenForOrders, Consumer
from Producer import Producer
from logger  import logger
from matchOrder import matches
import requests


def main():
    """
        This is the main file for doing 3 major things
        1. listen for orders
        2. Validate the orders with the subnet
        3. Process the order and send to meter to start producing
    """
    topic = 'validated-order'
    
    avroConsumer = Consumer()
    # avroProducer = Producer()
    # subnet = Subnet()

    while True:
        rawOrder = listenForOrders(avroConsumer, verbose=False)

        if rawOrder == None:
            continue
            
        validated = False

        if rawOrder.topic() == 'new-buy-order':
            #validation will be checking with "rate", "timelines" of matched sell orders"

            logger.info("Topic: {}".format(rawOrder.topic()))
            # logger.info(rawOrder.value())
            buyOrder = rawOrder.value()
            subnetID = buyOrder["subnet_id"]
            rate = buyOrder['rate']
            ordersBySubnet = requests.get("http://dos.grit.systems:5900/sell/by-subnet/{}/".format(subnetID), cookies={"role_id":"6"})
            orderJson = ordersBySubnet.json()
            
            if not orderJson['success']:
                logger.info(orderJson['message'])
                continue

            matchCount = orderJson["count"]
            sellOrders = orderJson["data"]
            logger.info("Buy order with rate: {} matches {} sell orders with subnetID {}".format(rate, matchCount, subnetID))
        
            if matchCount == 0:
                continue
            
            buyer_id = 0
            seller_id = 0
            estimated_value = 0
            matched = False
            for sellOrder in sellOrders:
                # logger.info("Rate: {}, Sell Rate: {}".format(int(rate), int(sellOrder['rate'])))
                if int(rate) == int(sellOrder['rate']):
                    #order matched, continue to save order information for tx api and blockchain api
                    logger.info("Order matched")
                    buyer_id = buyOrder['user_group_id']
                    seller_id = sellOrder['user_group_id']
                    estimated_value = rate * buyOrder['cost_limit']
                    matched = True
                    break
            
            if not matched: 
                logger.info("No matched Order")
                continue

            tx_data = {
                "buyer_id": buyer_id, 
                "seller_id": seller_id, 
                "estimated_value": int(estimated_value)
            }
            # tx_api = requests.post("http://dos.grit.systems:5900/transaction-api/new/", data=tx_data, cookies={"role_id":"6"})
            tx_api = requests.post("http://transaction_api:5000/transaction-api/new/", data=tx_data, cookies={"role_id":"6"})
            tx_response = tx_api.json()
            logger.info("Transaction API response: \n{}".format(tx_api.json()))

            if not tx_response['success']:
                logger.info("Transaction not saved to tx_api")
                continue
            
            order_id = tx_response['data']['id']
            logger.info("Order ID: \n{}".format(order_id))

            blockchain_request = {
                "order_id": int(order_id) + 50,
                "buyer_id": buyer_id,
                "seller_id": seller_id,
                "estimated_value": int(estimated_value),
                "other_details": "other"
            }
            # logger.info("Blockchain request: \n{}".format(blockchain_request))

            response = requests.post("http://blockchain_api:5000/new-order", json=blockchain_request)
            # logger.info("Blockchain response: {}".format(response.json()))

            blockchain_resp = response.json()
            if not blockchain_resp['success']:
                logger.info("Blockchain response: {}".format(blockchain_resp['message']))
            else:
                validated = True
            
        elif rawOrder.topic() == 'new-sell-order':
            #validation will be checking with subnet capacity

            logger.info("Topic: {}".format(rawOrder.topic()))
            # logger.info(rawOrder.value())
            # validated = subnet.validateSellOrder(rawOrder.value())

        elif rawOrder.topic() == 'order-complete':
            logger.info("Topic: {}".format(rawOrder.topic()))
            #notify blockchain of completion

        # logger.info("Available Subnet Capacity: {}".format(subnet.available))
        # logger.info("Current Subnet Capacity: {}".format(subnet.unused_capacity))
        # logger.info("Consumed Subnet Capacity: {}\n".format(subnet.consumed))

        if validated:  #validated and producing to device
            # avroProducer.produce(topic=topic, value=rawOrder.value())
            # avroProducer.flush()
            logger.info("Order validated and created to topic '{}'".format(topic))
        else:
            logger.info("Order not validated")
        

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass